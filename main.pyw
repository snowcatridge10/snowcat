from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtWebEngineWidgets import *
from PyQt5.QtPrintSupport import *
from PyQt5.QtWebEngineCore import QWebEngineUrlRequestInterceptor, QWebEngineUrlSchemeHandler, QWebEngineUrlScheme

import urllib.parse
import os
import sys
import json
import validators
import feedparser
from functools import partial

class AboutDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(AboutDialog, self).__init__(*args, **kwargs)

        QBtn = QDialogButtonBox.Ok  # No cancel
        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        layout = QVBoxLayout()

        title = QLabel("Snowcat")
        font = title.font()
        font.setPointSize(20)
        title.setFont(font)

        layout.addWidget(title)

        logo = QLabel()
        logo.setPixmap(QPixmap(os.path.join(snowcat_path, 'images', 'snowcat-icon-64.png')))
        layout.addWidget(logo)

        layout.addWidget(QLabel("Version 0.1"))

        for i in range(0, layout.count()):
            layout.itemAt(i).setAlignment(Qt.AlignHCenter)

        layout.addWidget(self.buttonBox)

        self.setLayout(layout)

class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.tabs = QTabWidget()
        self.tabs.setDocumentMode(True)
        self.tabs.tabBarDoubleClicked.connect(self.tab_open_doubleclick)
        self.tabs.currentChanged.connect(self.current_tab_changed)
        self.tabs.setTabsClosable(True)
        self.tabs.tabCloseRequested.connect(self.close_current_tab)
        self.tabs.setStyleSheet("""
                        QTabWidget {
                            background: #ffffff; 
                        }
                        QTabBar {
                            background: #e7eaed; 
                        }
                        QTabBar::tab {
                            background: #e7eaed; 
                            padding: 5px;
                            color: #000000;
                            margin-top: -1px;
                            margin-bottom: -1px; 
                            margin-left: 1pt solid black;
                            margin-right: 1pt solid black;
                            border: 1px solid #000000;
                            border-radius: 4px;
                        } 
                        QTabBar::tab:hover { 
                            background: #f0f0f0;  
                        } 
                        QTabBar::tab:selected { 
                            background: #ffffff;  
                            color: #000000;
                        }
                        """)

        self.setCentralWidget(self.tabs)

        self.status = QStatusBar()
        self.setStatusBar(self.status)

        self.cmd_prompt = QLineEdit()
        self.cmd_prompt.returnPressed.connect(self.do_cmd)
        self.statusBar().addWidget(self.cmd_prompt)
        self.cmd_prompt.hide()

        navtb = QToolBar("Navigation")
        navtb.setIconSize(QSize(16, 16))
        self.addToolBar(navtb)

        back_btn = QAction(QIcon(os.path.join(snowcat_path, 'images', 'arrow-180.png')), "Back", self)
        back_btn.setStatusTip("Back to previous page")
        back_btn.triggered.connect(lambda: self.tabs.currentWidget().back())
        navtb.addAction(back_btn)

        next_btn = QAction(QIcon(os.path.join(snowcat_path, 'images', 'arrow-000.png')), "Forward", self)
        next_btn.setStatusTip("Forward to next page")
        next_btn.triggered.connect(lambda: self.tabs.currentWidget().forward())
        navtb.addAction(next_btn)

        reload_btn = QAction(QIcon(os.path.join(snowcat_path, 'images', 'arrow-circle-315.png')), "Reload", self)
        reload_btn.setStatusTip("Reload page")
        reload_btn.triggered.connect(lambda: self.tabs.currentWidget().reload())
        navtb.addAction(reload_btn)

        home_btn = QAction(QIcon(os.path.join(snowcat_path, 'images', 'home.png')), "Home", self)
        home_btn.setStatusTip("Go home")
        home_btn.triggered.connect(self.navigate_home)
        navtb.addAction(home_btn)

        navtb.addSeparator()

        self.httpsicon = QLabel()  # Yes, really!
        self.httpsicon.setPixmap(QPixmap(os.path.join(snowcat_path, 'images', 'lock-nossl.png')))
        navtb.addWidget(self.httpsicon)

        self.urlbar = QLineEdit()
        self.urlbar.returnPressed.connect(self.navigate_to_url)
        self.urlbar.setFont(QFont("Arial", 10))
        navtb.addWidget(self.urlbar)

        add_bookmark_btn = QAction(QIcon(os.path.join(snowcat_path, 'images', 'star.png')), "Add a new bookmark", self)
        add_bookmark_btn.setStatusTip("Add a new bookmark")
        add_bookmark_btn.triggered.connect(self.add_bookmark)
        navtb.addAction(add_bookmark_btn)

        stop_btn = QAction(QIcon(os.path.join(snowcat_path, 'images', 'cross-circle.png')), "Stop", self)
        stop_btn.setStatusTip("Stop loading current page")
        stop_btn.triggered.connect(lambda: self.tabs.currentWidget().stop())
        navtb.addAction(stop_btn)

        if config["show_experimental_buttons"] == "yes":
            rss_btn = QAction(QIcon(os.path.join(snowcat_path, 'images', 'rss-solid.png')), "Rss", self)
            rss_btn.setStatusTip("Stop loading current page")
            rss_btn.triggered.connect(lambda: self.display_rss_feeds())
            navtb.addAction(rss_btn)

        menu = QMenu(self)
        new_tab_action = QAction(
            QIcon(os.path.join('images', 'ui-tab--plus.png')), "New Tab", self)
        new_tab_action.triggered.connect(lambda _: self.add_new_tab())
        menu.addAction(new_tab_action)

        open_file_action = QAction(
            QIcon(os.path.join('images', 'disk--arrow.png')), "Open file...", self)
        open_file_action.triggered.connect(self.open_file)
        menu.addAction(open_file_action)

        webdev_tools_action = QAction(QIcon(os.path.join('images', 'wrench.png')), "Open Web Developer Tools", self)
        webdev_tools_action.triggered.connect(self.open_webdev_tools)
        menu.addAction(webdev_tools_action)

        history_action = QAction("History", self)
        history_action.triggered.connect(self.history)
        menu.addAction(history_action)

        about_action = QAction(QIcon(os.path.join('images', 'question.png')), "About Snowcat", self)
        about_action.triggered.connect(self.about)
        menu.addAction(about_action)

        option_btn = QAction(
            QIcon(os.path.join(snowcat_path, 'images', 'blank.png')), "Option", self)
        option_btn.setMenu(menu)
        navtb.addAction(option_btn)

        # Uncomment to disable native menubar on Mac
        # self.menuBar().setNativeMenuBar(False)

        file_menu = self.menuBar().addMenu("&File")

        new_tab_action = QAction(QIcon(os.path.join(snowcat_path, 'images', 'ui-tab--plus.png')), "New Tab", self)
        new_tab_action.setStatusTip("Open a new tab")
        new_tab_action.triggered.connect(lambda _: self.add_new_tab())
        file_menu.addAction(new_tab_action)

        open_file_action = QAction(QIcon(os.path.join(snowcat_path, 'images', 'disk--arrow.png')), "Open file...", self)
        open_file_action.setStatusTip("Open from file")
        open_file_action.triggered.connect(self.open_file)
        file_menu.addAction(open_file_action)

        save_file_action = QAction(QIcon(os.path.join(snowcat_path, 'images', 'disk--pencil.png')), "Save Page As...", self)
        save_file_action.setStatusTip("Save current page to file")
        save_file_action.triggered.connect(self.save_file)
        file_menu.addAction(save_file_action)

        print_action = QAction(QIcon(os.path.join(snowcat_path, 'images', 'printer.png')), "Print...", self)
        print_action.setStatusTip("Print current page")
        print_action.triggered.connect(self.print_page)
        file_menu.addAction(print_action)

        bookmarks_menu = self.menuBar().addMenu("&Bookmarks")

        bookmarks_file = open(snowcat_path + 'bookmarks.txt', 'r')
        bookmarks = bookmarks_file.readlines()

        for bookmark in range(0, len(bookmarks)):
                locals()["bookmark_action_" + bookmarks[bookmark]] = QAction(bookmarks[bookmark], self)
                locals()["bookmark_action_" + bookmarks[bookmark]].setStatusTip("Open " + bookmarks[bookmark])
                locals()["bookmark_action_" + bookmarks[bookmark]].triggered.connect(partial(self.open_bookmark, bookmarks[bookmark]))
                bookmarks_menu.addAction(locals()["bookmark_action_" + bookmarks[bookmark]])
        
        bookmarks_file.close()

        help_menu = self.menuBar().addMenu("&Help")

        about_action = QAction(QIcon(os.path.join(snowcat_path, 'images', 'question.png')), "About Snowcat", self)
        about_action.setStatusTip("Find out more about Snowcat")
        about_action.triggered.connect(self.about)
        help_menu.addAction(about_action)

        self.printer = QPrinter()
        
        try:
            if validators.url(sys.argv[1]):
                self.add_new_tab(QUrl(sys.argv[1]), '')
            else:
                self.add_new_tab(QUrl(homepage), 'Homepage')
        except:
            self.add_new_tab(QUrl(homepage), 'Homepage')

        self.shortcut = QShortcut(QKeySequence("Ctrl+T"), self)
        self.shortcut.activated.connect(self.add_new_tab)

        self.shortcut = QShortcut(QKeySequence("Ctrl+R"), self)
        self.shortcut.activated.connect(lambda: self.tabs.currentWidget().reload())

        self.shortcut = QShortcut(QKeySequence("Ctrl+W"), self)
        self.shortcut.activated.connect(lambda: self.close_current_tab(self.tabs.currentIndex()))

        self.shortcut = QShortcut(QKeySequence("Ctrl+;"), self)
        self.shortcut.activated.connect(self.toggle_cmd_prompt)

        self.shortcut = QShortcut(QKeySequence("Ctrl+U"), self)
        self.shortcut.activated.connect(self.view_source)

        # opening window in maximized size
        self.showMaximized()

        self.show()

        self.setWindowTitle("Snowcat")
        self.setWindowIcon(QIcon(os.path.join(snowcat_path, 'images', 'snowcat-icon-64.png')))

    def open_webdev_tools(self):
        with open("devConsole.js") as my_file:
            self.tabs.currentWidget().page().runJavaScript(my_file.read())

    def add_bookmark(self):
        currentTabUrl = self.tabs.currentWidget().url().toString()

        bookmarksFile = open(snowcat_path + 'bookmarks.txt', 'r')
        bookmarksFileLines = bookmarksFile.readlines()

        containsBookmark = False
        for bookmarksFileLine in bookmarksFileLines:
            if bookmarksFileLine.strip() == currentTabUrl.strip():
                containsBookmark = True
                break

        bookmarksFile.close()

        if not containsBookmark:
            with open(snowcat_path + 'bookmarks.txt', 'a') as file:
                file.write('\n' + currentTabUrl)
    
    def view_source(self):
        url = self.urlbar.text()
        if not url.startswith('view-source:'):
            self.add_new_tab(QUrl("view-source:" + url))

    def open_bookmark(self, bookmark):
        self.add_new_tab(QUrl(bookmark.strip()))

    def do_cmd(self):
        cmd = self.cmd_prompt.text()

        if cmd == "quit":
            sys.exit()
        elif cmd == "newtab":
            self.add_new_tab()
        elif cmd == "reload":
            self.tabs.currentWidget().reload()
        elif cmd == "close_current_tab":
            self.close_current_tab(self.tabs.currentIndex())
        elif cmd == "new_window":
            self.show_new_window()
        elif cmd.startswith("o "):
            self.navigate_to_url(cmd.replace("o ", ""))
        elif cmd == "add_bookmark":
            self.add_bookmark()
        elif cmd == "":
            self.status.showMessage("No command given", 2000)
        else:
            self.status.showMessage(cmd + ": no such command", 2000)

        self.cmd_prompt.setText("")
        self.cmd_prompt.hide()

    def toggle_cmd_prompt(self):
        self.cmd_prompt.show()
        self.cmd_prompt.setFocus()

    @pyqtSlot("QWebEngineDownloadItem*")
    def on_downloadRequested(self, download):
        old_path = download.url().path()  # download.path()
        suffix = QFileInfo(old_path).suffix()
        path, _ = QFileDialog.getSaveFileName(
            self, "Save File", old_path, "*." + suffix
        )
        if path:
            download.setPath(path)
            download.accept()

    def add_new_tab(self, qurl=None, label="New Tab"):

        if qurl is None:
            qurl = QUrl(homepage)

        browser = QWebEngineView()
        browser.setPage(WebPage(self))
        QWebEngineProfile.defaultProfile().downloadRequested.connect(self.on_downloadRequested)
        browser.setUrl(qurl)
        newtabI = self.tabs.addTab(browser, label)
        newtab = self.tabs.widget(newtabI)

        self.tabs.setTabIcon(newtabI, browser.page().icon())

        url = qurl.toString()
        if qurl.scheme() == "snowcat":
            html = ""
            if url.split("/")[2] == "about":
                with open(snowcat_path + "about.html", 'r') as f:
                    html = f.read()
            elif url.split("/")[2] == "history":
                for j in self.tabs.currentWidget().page().history().backItems(1000):
                    html = "<br>" + html
                    html = "<p>" + j.url().toString() + "</p>" + html
                    html = "<h2>" + j.title() + "</h2>" + html

            newtab.setHtml(html)
        else:
            # More difficult! We only want to update the url when it's from the
            # correct tab
            browser.urlChanged.connect(lambda qurl, browser=browser:
                                    self.update_urlbar(qurl, browser))

            browser.loadFinished.connect(lambda _, i=newtabI, browser=browser:
                                        self.tabs.setTabText(newtabI, browser.page().title()))
            
            browser.iconChanged.connect(lambda _, i=newtabI, browser=browser:
                                     self.tabs.setTabIcon(newtabI, browser.icon()))

        self.tabs.setCurrentIndex(newtabI)

    def tab_open_doubleclick(self, i):
        if i == -1:  # No tab under the click
            self.add_new_tab()

    def current_tab_changed(self, i):
        qurl = self.tabs.currentWidget().url()
        self.update_urlbar(qurl, self.tabs.currentWidget())
        self.update_title(self.tabs.currentWidget())

    def close_current_tab(self, i):
        if self.tabs.count() < 2:
            return

        self.tabs.removeTab(i)

    def update_title(self, browser):
        if browser != self.tabs.currentWidget():
            # If this signal is not from the current tab, ignore
            return

        title = self.tabs.currentWidget().page().title()
        self.setWindowTitle("%s - Snowcat" % title)

    def about(self):
        self.add_new_tab(QUrl("snowcat://about"), "About Snowcat")
    
    def history(self):
        self.add_new_tab(QUrl("snowcat://history"), "History")

    def open_file(self):
        filename, _ = QFileDialog.getOpenFileName(self, "Open file", "",
                                                  "Hypertext Markup Language (*.htm *.html);;"
                                                  "All files (*.*)")

        if filename:
            with open(filename, 'r') as f:
                html = f.read()

            self.tabs.currentWidget().setHtml(html)
            self.urlbar.setText(filename)

    def save_file(self):
        filename, _ = QFileDialog.getSaveFileName(self, "Save Page As", "",
                                                  "Hypertext Markup Language (*.htm *html);;"
                                                  "All files (*.*)")

        if filename:
            html = self.tabs.currentWidget().page().mainFrame().toHtml()
            with open(filename, 'w') as f:
                f.write(html.encode('utf8'))

    def print_page(self):
        dlg = QPrintDialog(self.printer)
        if dlg.exec_():
            self.browser.page().print(self.printer, self.print_completed)
    
    def print_completed(self, success):
        self.status.showMessage(success, 2000)

    def navigate_home(self):
        self.tabs.currentWidget().setUrl(QUrl(homepage))

    def navigate_to_url(self, url=None):
        if url is None:
            url = self.urlbar.text()

        if url.startswith(":yt "):
            url = url.replace(":yt ", "https://yewtu.be/search?q=")

        q = QUrl(url)
        if q.scheme() == "":
            if validators.url("http://" + url):
                q = QUrl("http://" + url)
            else:
                q = QUrl(search_engine_url + url)

        self.tabs.currentWidget().setUrl(q)

    def update_urlbar(self, q, browser=None):

        if browser != self.tabs.currentWidget():
            # If this signal is not from the current tab, ignore
            return

        if q.scheme() == 'https':
            # Secure padlock icon
            self.httpsicon.setPixmap(QPixmap(os.path.join(snowcat_path, 'images', 'lock-ssl.png')))

        else:
            # Insecure padlock icon
            self.httpsicon.setPixmap(QPixmap(os.path.join(snowcat_path, 'images', 'lock-nossl.png')))

        self.urlbar.setText(q.toString())
        self.urlbar.setCursorPosition(0)
    
    def display_rss_feeds(self):
        self.should_show_rss_msg = True
        rss_msg_box = QMessageBox()
        rss_msg_box.setIcon(QMessageBox.Information)
        rss_msg_box.buttonClicked.connect(self.rss_feed_button_pressed)
        rss_msg_box.addButton(QPushButton('Next'), QMessageBox.YesRole)
        rss_msg_box.addButton(QPushButton('Cancel'), QMessageBox.RejectRole)

        rssFeeds = open(snowcat_path + 'rssFeeds.txt', 'r')
        rssFeedLines = rssFeeds.readlines()

        for line in rssFeedLines:
            NewsFeed = feedparser.parse(line)

            for entry in NewsFeed.entries:
                try:
                    if self.should_show_rss_msg == False:
                        break
                    rss_msg_box.setText(entry.published + "\n" + "------Summary------" + "\n" + entry.summary + "\n" + "------Link------" + "\n" + entry.link)
                    retval = rss_msg_box.exec_()
                except:
                    pass

    def rss_feed_button_pressed(self, i):
        if i.text() == "Cancel":
            self.should_show_rss_msg = False

class WebPage(QWebEnginePage):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.featurePermissionRequested.connect(
            self.handleFeaturePermissionRequested)

    @pyqtSlot(QUrl, QWebEnginePage.Feature)
    def handleFeaturePermissionRequested(self, securityOrigin, feature):
        title = "Permission Request"
        questionForFeature = {
            QWebEnginePage.Geolocation: "Allow {feature} to access your location information?",
            QWebEnginePage.MediaAudioCapture: "Allow {feature} to access your microphone?",
            QWebEnginePage.MediaVideoCapture: "Allow {feature} to access your webcam?",
            QWebEnginePage.MediaAudioVideoCapture: "Allow {feature} to access your webcam and microphone?",
            QWebEnginePage.DesktopVideoCapture: "Allow {feature} to capture video of your desktop?",
            QWebEnginePage.DesktopAudioVideoCapture: "Allow {feature} to capture audio and video of your desktop?"
        }
        question = questionForFeature.get(feature)
        if question:
            question = question.format(feature=securityOrigin.host())
            if QMessageBox.question(self.view().window(), title, question) == QMessageBox.Yes:
                self.setFeaturePermission(
                    securityOrigin, feature, QWebEnginePage.PermissionGrantedByUser)
            else:
                self.setFeaturePermission(
                    securityOrigin, feature, QWebEnginePage.PermissionDeniedByUser)

    def acceptNavigationRequest(self, url,  _type, isMainFrame):
        urlString = url.toString()
        redirecting = False

        if "youtube.com" in urlString:
            urlString = urlString.replace("youtube.com", "yewtu.be")
            redirecting = True
        elif "translate.google.com" in urlString:
            urlString = urlString.replace("translate.google.com", "simplytranslate.org")
            redirecting = True
        if redirecting:
            window.navigate_to_url(urlString)

        path = snowcat_path + 'userscripts'
        list_of_files = []

        for root, dirs, files in os.walk(path):
            for file in files:
                list_of_files.append(os.path.join(root,file))
        for name in list_of_files:
            with open(name) as my_file:
                self.runJavaScript(my_file.read())

        return True
    
    def createWindow(self, type_):
        if not isinstance(window, MainWindow):
            return

        if type_ == QWebEnginePage.WebBrowserTab:
            return window.add_new_tab(QUrl("file:///feature_unavailable.html"))

app = QApplication(sys.argv)
app.setApplicationName("Snowcat")
app.setOrganizationName("Aidan")

snowcat_path = "C:/Projects/snowcat/"

with open(snowcat_path + 'config.json', 'r') as f:
    config = json.loads(f.read())

if config['theme'] != "default":
    with open(snowcat_path + config['theme'], 'r') as f:
        style = f.read()
        # Set the stylesheet of the application
        app.setStyleSheet(style)

if config['homepage'] == "default":
    homepage = "http://localhost"
else:
    homepage = config['homepage']

if config['search_engine_url'] == "default":
    search_engine_url = "https://duckduckgo.com/?q="
else:
    search_engine_url = config['search_engine_url']

window = MainWindow()
app.exec_()
